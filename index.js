// Lesson 3
function fizzBuzz (number) {
    for (let i = 1; i <= number; i++) {
        if (i % 3 === 0 && i % 5 === 0) console.log(`${i} => FizzBuzz `);
        else if (i % 3 === 0) console.log(`${i} => Fizz `);
        else if (i % 5 === 0) console.log(`${i} => Buzz `);
    }
};
fizzBuzz(100);
//------------------------------------------------------------------

function filterArray (list) {
    // your code here
    // let formatter = list.filter(el => el.length && typeof el !== 'string' ? true : false )
    // formatter = new Set(formatter.reduce((acc, curr) => [...acc, ...curr], [2, 7,8,9]))
    // return Array.from(formatter).sort((a,b) => b-a )
    // var arr1 = [[2], 23, 'dance', true, [3, 5, 3]],
    // arr2 = [23, 'dance', true],
    // res = arr1.filter(item => !arr2.includes(item));
    // console.log(res);
}
// const fruits = ['apple', 'banana']

// const vegetables = [ ...fruits ] // 0 index
    // console.log(vegetables)
// Output DON'T TOUCH!
// console.log(fizzBuzz())
const letList = [
    [2], 
    23,
    'dance',
    [7,8,9, 7],
    true, 
    [3, 5, 3],
    [777,855, 9677, 457],
]
// console.log(filterArray(letList)) // 2,3,5,7,8,9

const result = [];
function filterArray2(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].length && typeof arr[i] === "object") {
      for (var k = 0; k < arr[i].length; k++) {
        if(!result.includes(arr[i][k]))
        result.push(arr[i][k]);
      }
    }
  }
  console.log(result.sort((a,b)=>a-b));
}
filterArray2(letList);
